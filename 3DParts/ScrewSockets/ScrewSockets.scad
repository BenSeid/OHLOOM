/*File Info--------------------------------------------------------------------
File Name: ScrewSockets.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------

h1=3;
h2=10;
h3=16;
d1=14;
d2=8;
d3=4;
reso=80;
screwhole=true;


if(screwhole)
{
    union()
    {
        difference()
        {
            union()
            {
                cylinder(h=h1,d=d1,$fn=reso);
                translate([0,0,h1])
                rotate([0,0,45])
                cylinder(h=h2,d=d2,$fn=4);
            }
            union()
            {
                cylinder(h=h1,d1=d3+6,d2=3,$fn=reso);
                cylinder(h=h3,d=d3,$fn=reso);
            }
        }
    }
}
else
{
    union()
    {
        difference()
        {
            union()
            {
                cylinder(h=h1,d=d1,$fn=reso);
                translate([0,0,h1])
                rotate([0,0,45])
                cylinder(h=h2,d=d2,$fn=4);
            }
            cylinder(h=h3,d=d3,$fn=reso);
        }
    }
}    
