# OHLOOM - Montageanleitung \[GER\]

[**\[ENG\]**](AssemblyGuide.md)

![](Assembly_Guide.jpg)


## Index

1. Montageteile
   1. [Metallteile](#metal-parts)
   2. [3D Druckteile](#print-parts)
   3. [CNC Teile](#cnc-parts)
   4. [Andere Teile](#other-parts)
2. Montagevorbereitung
   1. [Farbiger Schutzfilm](#protection)
3. [Montage](#assembly)


# 1. Montageteile

## 1.1. Metallteile {#metal-parts}

| | | |
| :----: | :----: | :----: |
| ![](Parts_Metal_1.jpg) 2 x Gewindebolzen M8 x 180 mm |  ![](Parts_Metal_2.jpg) 4 x M8 Muttern |  ![](Parts_Metal_3.jpg) 2 x M8 Muttern mit Gummiring |
| ![](Parts_Metal_4.jpg) 4 x M6 Muttern |  ![](Parts_Metal_5.jpg) 4 x M6 Zylinderschrauben |
| ![](Parts_Metal_6.jpg) 8 x W60x4 Holzschrauben |  ![](Parts_Metal_7.jpg) 4 x W35x4 Holzschrauben |  ![](Parts_Metal_8.jpg) 16 x W12x4 Holzschrauben |


## 1.2. 3D Druckteile {#print-parts}

| | | |
| :----: | :----: | :----: |
| ![](Parts_Print_1.jpg) 2 x Sperrrad |  ![](Parts_Print_2.jpg) 2 x Klemmenring |  ![](Parts_Print_3.jpg) 5 x Kammmodul |
| ![](Parts_Print_4.jpg) 2 x Sperrkeil |  ![](Parts_Print_5.jpg) 12 x Schraubenfuss mit |  ![](Parts_Print_6.jpg) 4 x Schraubenfuss ohne |
| ![](Parts_Print_7.jpg) 2 Achtkantwellenelemente (4 pro Einheit) |


## 1.3. CNC Teile (Holz) {#cnc-parts}

| | | |
| :----: | :----: | :----: |
| ![](Parts_CNC_1.jpg) 2 x Kammhalter |  ![](Parts_CNC_2.jpg) 2 x Seitenteil |  ![](Parts_CNC_3.jpg) 2 x Querteil |
| ![](Parts_CNC_4.jpg) 2 x Einschnittleiste |  ![](Parts_CNC_5.jpg) 2 x Schnurleiste |  ![](Parts_CNC_6.jpg) 1 x Schiff |


## 1.4. Andere Teile {#other-parts}

| | |
| :----: | :----: |
| ![](Parts_Other_1.jpg) 2 x Holzwelle |  ![](Parts_Other_2.jpg) ca. 3 m Schnur |


# 2. Montagevorbereitung

## 2.1. Farbiger Schutzfilm {#protection}

| |
| :----: |
| ![](Assembly_Preparation_1.jpg) |
| 1. Man braucht Farbpigmente, Leinöl, ein leeres Glass und Pinsel  ![](Assembly_Preparation_2.jpg) |
| 2. Zuerst fülle die Pigmente ins Glas (ca. 10-20 Gramm).  ![](Assembly_Preparation_3.jpg) |
| 3. Fülle als nächstes 8-10 mal soviel Öl wie Pigmente ins Glass und rühre kräftig.  ![](Assembly_Preparation_4.jpg) |
| 4. Rühr solang die Pigmente bis sie gut vermischt sind und keine Klümpchen mehr aufweisen.  ![](Assembly_Preparation_5.jpg) |
| 5. Die Holzoberfläche sollte sauber und trocken sein. |
| 6. Trage die Farbe in 1-3  Schichten auf, bis es das gewünschte  Aussehen aufweist. |
| 7. Lass es über Nacht trocknen. |


# 3. Montage {#assembly}

| | | | |
|-|----|----|----|
| 1.  | ![](Assembly_1.jpg)  | ![](Assembly_2.jpg)  |
| 2.  | ![](Assembly_3.jpg)  | ![](Assembly_4.jpg)  |
| 3.  | ![](Assembly_5.jpg)  | ![](Assembly_6.jpg)  |
| 4.  | ![](Assembly_7.jpg)  | ![](Assembly_8.jpg)  |
| 5.  | ![](Assembly_9.jpg)  | ![](Assembly_10.jpg) |
| 6.  | ![](Assembly_11.jpg) | ![](Assembly_12.jpg) |
| 7.  | ![](Assembly_13.jpg) | ![](Assembly_14.jpg) | ![](Assembly_15.jpg) |
| 8.  | ![](Assembly_16.jpg) | ![](Assembly_17.jpg) | ![](Assembly_18.jpg) |
| 9.  | ![](Assembly_19.jpg) | ![](Assembly_20.jpg) | ![](Assembly_21.jpg) |
| 10. | ![](Assembly_22.jpg) | ![](Assembly_23.jpg) |
| 11. | ![](Assembly_24.jpg) |
| 12. | ![](Assembly_25.jpg) |

Voila! Dein erster OHLOOM Webstuhl ist zusammengebaut. Glückwunsch!
